package co.bakko.quiz.view.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import co.bakko.quiz.R;
import co.bakko.quiz.callback.SnackBarCallback;
import co.bakko.quiz.view.fragment.LoginFragment;
import co.bakko.quiz.view.fragment.SignUpFragment;

public class AuthActivity extends AppCompatActivity implements SnackBarCallback {

    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;
    private TabLayout tabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);

        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        tabLayout.setupWithViewPager(mViewPager);
    }

    @Override
    public void showSnackBar(String msg) {
        Snackbar.make(findViewById(R.id.main_content), msg, Snackbar.LENGTH_LONG).setAction("Action", null).show();
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {

            switch (position) {
                case 0: return LoginFragment.newInstance();

                case 1: return SignUpFragment.newInstance();
            }
            return null;
        }

        @Override
        public int getCount() {
            // Show 2 total pages.
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "LOGIN";
                case 1:
                    return "SIGN UP";
            }
            return null;
        }
    }

    public static void start(Context context) {
        Intent starter = new Intent(context, AuthActivity.class);
        context.startActivity(starter);
        ((Activity)context).finish();
    }
}
