package co.bakko.quiz.view.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import co.bakko.quiz.R;
import co.bakko.quiz.callback.SnackBarCallback;
import co.bakko.quiz.contact.MainViewContact;
import co.bakko.quiz.databinding.FragmentMainBinding;
import co.bakko.quiz.model.UserInfo;
import co.bakko.quiz.presenter.MainViewPresenter;
import co.bakko.quiz.presenter.PhoneNoUpdatePresenter;
import co.bakko.quiz.view.dialog.PhoneNoEditDialog;
import co.bakko.quiz.view.activity.SplashScreenActivity;
import co.bakko.quiz.view.activity.MainViewActivity;

public class MainViewFragment extends Fragment implements MainViewContact.View{

    private static final String ARG_USER_ID  = "user_id";

    private MainViewContact.UserActionListener userActionListener;
    private FragmentMainBinding bindingView;

    private String emailId;
    private SnackBarCallback callback;

    public MainViewFragment() { }

    public static MainViewFragment newInstance(String userId) {
        MainViewFragment fragment = new MainViewFragment();
        Bundle args = new Bundle();
        args.putString(ARG_USER_ID, userId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof MainViewActivity) {
            callback = (MainViewActivity) context;
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            emailId = getArguments().getString(ARG_USER_ID);
        }

        userActionListener = new MainViewPresenter(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        bindingView = DataBindingUtil.inflate(inflater, R.layout.fragment_main, container, false);
        userActionListener.onStart(emailId);

        bindingView.btnUserType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                userActionListener.userTypeAction();
            }
        });
        bindingView.btnLogOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                userActionListener.logout();
                bindingView.btnLogOut.setEnabled(false);
            }
        });

        bindingView.btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialogToUpdatePhoneNo();
            }
        });

        return bindingView.getRoot();
    }

    @Override
    public void onResume() {
        userActionListener.onStart(emailId);
        super.onResume();
    }


    @Override
    public void showUserInformation(UserInfo userInfo) {
        bindingView.textViewFirstName.setText(userInfo.getFirstName());
        bindingView.textViewLastName.setText(userInfo.getLastName());
        bindingView.textViewMobileNo.setText(userInfo.getPhoneNo());
    }

    @Override
    public void showToastMsg(String msg) {
        Toast.makeText(getContext(), msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showMsgInSnackBar(String msg) {
        callback.showSnackBar(msg);
    }

    @Override
    public void showDialogToUpdatePhoneNo() {
        PhoneNoEditDialog dialog = PhoneNoEditDialog.newInstance(emailId);
        dialog.setTargetFragment(this, PhoneNoUpdatePresenter.REQUEST_CODE);
        dialog.show(getFragmentManager(), "phone_edit");
    }

    @Override
    public void showProgressDialog() {
        final ProgressDialog dialog = ProgressDialog.show(getContext(), "", "Processing for Logout");
        dialog.show();
        new Thread() {
            public void run() {
                try {
                    Thread.sleep(5000);
                    dialog.dismiss();
                    forwardToSplashScreen();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }.start();

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PhoneNoUpdatePresenter.REQUEST_CODE) {
            String updatedPhoneNo = data.getStringExtra(PhoneNoUpdatePresenter.EDIT_TEXT_BUNDLE_KEY);
            bindingView.textViewMobileNo.setText(updatedPhoneNo);
        }
    }

    @Override
    public void forwardToSplashScreen() {
        SplashScreenActivity.start(getContext());
    }

    @Override
    public void onDetach() {
        super.onDetach();
        callback = null;
    }
}
