package co.bakko.quiz.presenter;

import android.content.Intent;
import android.support.v4.app.Fragment;

import co.bakko.quiz.callback.QuizCallback;
import co.bakko.quiz.contact.PhoneNoUpdateContact;
import co.bakko.quiz.realm.dao.IUserDao;
import co.bakko.quiz.realm.dao.UserDao;
import co.bakko.quiz.util.Tools;

public class PhoneNoUpdatePresenter implements PhoneNoUpdateContact.UserActionListener {

    public static final String EDIT_TEXT_BUNDLE_KEY = "phone_no";
    public static int REQUEST_CODE = 0;

    private PhoneNoUpdateContact.View view;
    private IUserDao userRepo;
    private Fragment fragment;
    private String userEmailId;

    public PhoneNoUpdatePresenter(PhoneNoUpdateContact.View view) {
        this.view = view;
        this.userRepo = new UserDao();
    }

    @Override
    public void onStart(Fragment fragment, String emailId) {
        this.fragment = fragment;
        this.userEmailId = emailId;
    }

    @Override
    public void updatePhoneNo(final String phoneNo) {

        if (!Tools.isPhoneNoValid(phoneNo)) {
            view.showPhoneNoErrorMsg(true);
            return;
        }

        userRepo.updateUserPhoneNo(userEmailId, phoneNo, new QuizCallback<Boolean>() {
            @Override
            public void onSuccess(Boolean success) {
                sendIntentPhoneNo(phoneNo);
                view.dismissDialog();
            }

            @Override
            public void onError(String message) {
                view.dismissDialog();
            }
        });
    }

    @Override
    public void sendIntentPhoneNo(String phoneNo) {
        Intent intent = new Intent();
        intent.putExtra(EDIT_TEXT_BUNDLE_KEY, phoneNo);
        fragment.getTargetFragment().onActivityResult(fragment.getTargetRequestCode(), REQUEST_CODE, intent);
    }

    @Override
    public void onDestroy() {

    }
}
