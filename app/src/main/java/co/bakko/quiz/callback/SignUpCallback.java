package co.bakko.quiz.callback;

public interface SignUpCallback {
    void onSuccess(boolean userSaved, String userId);
    void onError(String message);
}
