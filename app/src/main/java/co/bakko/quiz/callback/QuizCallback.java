package co.bakko.quiz.callback;

public interface QuizCallback<T> {
    void onSuccess(T result);
    void onError(String message);
}
