package co.bakko.quiz.realm.module;

import co.bakko.quiz.model.AuthData;
import co.bakko.quiz.model.UserInfo;
import io.realm.annotations.RealmModule;

@RealmModule(classes = {UserInfo.class, AuthData.class})
public class QuizModule { }
