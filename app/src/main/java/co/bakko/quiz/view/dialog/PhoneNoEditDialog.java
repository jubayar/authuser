package co.bakko.quiz.view.dialog;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import co.bakko.quiz.R;
import co.bakko.quiz.contact.PhoneNoUpdateContact;
import co.bakko.quiz.presenter.PhoneNoUpdatePresenter;

public class PhoneNoEditDialog extends DialogFragment implements PhoneNoUpdateContact.View {

    private static final String TAG_USER_ID = "user_id";

    private EditText phoneNumber;
    private Button buttonUpdate;
    private View view;

    private String emailId;

    private PhoneNoUpdateContact.UserActionListener userActionListener;

    public PhoneNoEditDialog() {}

    public static PhoneNoEditDialog newInstance(String userId) {
        PhoneNoEditDialog fragment = new PhoneNoEditDialog();
        Bundle args = new Bundle();
        args.putString(TAG_USER_ID, userId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            emailId = getArguments().getString(TAG_USER_ID);
        }

        userActionListener = new PhoneNoUpdatePresenter(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.dialog_phone_no_update, container, false);
        phoneNumber = view.findViewById(R.id.phone_no);
        buttonUpdate = view.findViewById(R.id.btn_update);

        userActionListener.onStart(this, emailId);

        buttonUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                userActionListener.updatePhoneNo(phoneNumber.getText().toString());
            }
        });

        return view;
    }

    @Override
    public void showPhoneNoErrorMsg(boolean isError) {
        if (isError) {
            phoneNumber.setError("Mobile no is incorrect");
        } else {
            phoneNumber.setError(null);
        }
    }

    @Override
    public void dismissDialog() {
        dismiss();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

}
