package co.bakko.quiz.presenter;

import android.content.Context;

import co.bakko.quiz.R;
import co.bakko.quiz.callback.QuizCallback;
import co.bakko.quiz.contact.LoginContact;
import co.bakko.quiz.model.AuthData;
import co.bakko.quiz.realm.dao.AuthDao;
import co.bakko.quiz.realm.dao.IAuthDao;
import co.bakko.quiz.util.Tools;

public class LoginPresenter implements LoginContact.UserActionListener {

    private LoginContact.View view;
    private Context context;
    private IAuthDao authRepo;

    public LoginPresenter(LoginContact.View view) {
        this.view = view;
        authRepo = new AuthDao();
    }

    @Override
    public void onStart(Context context) {
         this.context = context;
    }

    @Override
    public void loginAction(String emailId, String password) {

        if (emailId.trim().length() == 0 && password.trim().length() == 0) {
            view.showMsgInSnackBar(context.getString(R.string.please_complete_the_form));
            return;
        }

        if (!Tools.isEmailValid(emailId)) {
            view.showEmailIdErrorMsg(true);
            return;
        }

        if (!Tools.isPasswordValid(password)) {
            view.showPasswordErrorMsg(true);
            return;
        }

        view.showProgressIndicator(true);

        authRepo.checkValidUser(new AuthData(emailId, password), new QuizCallback<String>() {
            @Override
            public void onSuccess(String userId) {
                view.forwardToMainView(userId);
            }

            @Override
            public void onError(String message) {
                view.showProgressIndicator(false);
                view.showMsgInSnackBar(message);
            }
        });

    }

    @Override
    public void onDestroy() {
        view = null;
        context = null;
        authRepo = null;
    }
}
