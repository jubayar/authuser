package co.bakko.quiz.model;

public enum UserType { Broker, Agent, Dealer, Private }
