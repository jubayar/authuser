package co.bakko.quiz.callback;

public interface SnackBarCallback {
    void showSnackBar(String msg);
}
