package co.bakko.quiz.view.fragment;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import co.bakko.quiz.R;
import co.bakko.quiz.callback.SnackBarCallback;
import co.bakko.quiz.contact.SignUpContact;
import co.bakko.quiz.databinding.FragmentSignUpBinding;
import co.bakko.quiz.model.UserInfo;
import co.bakko.quiz.model.UserType;
import co.bakko.quiz.presenter.SignUpPresenter;
import co.bakko.quiz.view.activity.AuthActivity;
import co.bakko.quiz.view.activity.MainViewActivity;

public class SignUpFragment extends Fragment implements SignUpContact.View, AdapterView.OnItemSelectedListener {

    private SignUpContact.UserActionListener userActionListener;

    private SnackBarCallback callback;
    private FragmentSignUpBinding bindingView;

    public SignUpFragment() {}

    public static SignUpFragment newInstance() {
        SignUpFragment fragment = new SignUpFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        userActionListener = new SignUpPresenter(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        bindingView = DataBindingUtil.inflate(inflater, R.layout.fragment_sign_up, container, false);

        bindingView.userSignUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UserInfo userInfo = new UserInfo();
                userInfo.setEmail(bindingView.editTextEmailId.getText().toString());
                userInfo.setPhoneNo(bindingView.editTextPhoneNo.getText().toString());
                userInfo.setPassword(bindingView.editTextPassword.getText().toString());
                userInfo.setFirstName(bindingView.editTextFirstName.getText().toString());
                userInfo.setLastName(bindingView.editTextLastName.getText().toString());
                userInfo.setType(UserType.valueOf(bindingView.userTypeSpinner.getSelectedItem().toString()));
                //
                userActionListener.signUpAction(userInfo);
            }
        });

        return bindingView.getRoot();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof AuthActivity)
            callback = (AuthActivity)context;
    }

    @Override
    public void onStart() {
        super.onStart();
        userActionListener.onStart(getContext());
    }

    @Override
    public void showErrorMsgForEmailId(boolean isError) {
        if (isError) {
            bindingView.editTextEmailId.setError(getString(R.string.error_invalid_email));
            bindingView.editTextEmailId.requestFocus();
        } else {
            bindingView.editTextEmailId.setError(null);
        }
    }

    @Override
    public void showErrorMsgForPassword(boolean isError) {
        if (isError) {
            bindingView.editTextPassword.setError(getString(R.string.error_incorrect_password));
            bindingView.editTextPassword.requestFocus();
        } else {
            bindingView.editTextPassword.setError(null);
        }
    }

    @Override
    public void showErrorMsgForPhoneNo(boolean isError) {
        if (isError) {
            bindingView.editTextPhoneNo.setError(getString(R.string.error_incorrect_mobile_no));
            bindingView.editTextPhoneNo.requestFocus();
        } else {
            bindingView.editTextPhoneNo.setError(null);
        }
    }

    @Override
    public void showMsgForMissingData(String msg) {
        callback.showSnackBar(msg);
    }

    @Override
    public void showSpinner() {
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(),
                R.array.user_type_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        bindingView.userTypeSpinner.setAdapter(adapter);
        bindingView.userTypeSpinner.setOnItemSelectedListener(this);
        bindingView.userTypeSpinner.setSelection(0);
    }

    @Override
    public void showProgressView(boolean show) {
        bindingView.signUpProgress.setVisibility(show ? View.VISIBLE : View.GONE);
        bindingView.emailLoginForm.setVisibility(show ? View.GONE : View.VISIBLE);
    }

    @Override
    public void forwardToMainView(String userId) {
        MainViewActivity.start(getContext(), userId);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        callback = null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        userActionListener.onDestroy();
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}
