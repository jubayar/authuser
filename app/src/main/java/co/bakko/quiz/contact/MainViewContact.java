package co.bakko.quiz.contact;

import co.bakko.quiz.model.UserInfo;

public interface MainViewContact {

    interface View {

        void showUserInformation(UserInfo userInfo);

        void showToastMsg(String msg);

        void showMsgInSnackBar(String msg);

        void showDialogToUpdatePhoneNo();

        void showProgressDialog();

        void forwardToSplashScreen();
    }

    interface UserActionListener {

        void onStart(String emailId);

        void editPhoneNoAction(String emailId, String password);

        void userTypeAction();

        void logout();

        void onDestroy();
    }
}
