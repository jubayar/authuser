package co.bakko.quiz.view.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import co.bakko.quiz.R;
import co.bakko.quiz.callback.SnackBarCallback;
import co.bakko.quiz.view.fragment.MainViewFragment;

public class MainViewActivity extends AppCompatActivity implements SnackBarCallback{

    private static String ARG_USER_ID = "user_id";

    private String userId;
    private Fragment fragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (getIntent().getExtras() != null) {
            userId = getIntent().getExtras().getString(ARG_USER_ID);
        }

        fragment = MainViewFragment.newInstance(userId);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.contentFrame, fragment);
        transaction.commit();
    }

    @Override
    public void showSnackBar(String msg) {
        Snackbar.make(findViewById(R.id.main_content), msg, Snackbar.LENGTH_LONG).setAction("Action", null).show();
    }

    public static void start(Context context, String userId) {
        Intent starter = new Intent(context, MainViewActivity.class);
        starter.putExtra(ARG_USER_ID, userId);
        context.startActivity(starter);
        ((Activity)context).finish();
    }
}
