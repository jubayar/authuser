package co.bakko.quiz.realm.dao;

import co.bakko.quiz.callback.QuizCallback;
import co.bakko.quiz.model.AuthData;

public interface IAuthDao {

    void checkValidUser(AuthData data, QuizCallback<String> callback);

    void addUserLoginData(AuthData data, QuizCallback<Boolean> callback);
}
