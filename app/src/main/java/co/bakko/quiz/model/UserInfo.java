package co.bakko.quiz.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

public class UserInfo extends RealmObject{

    @PrimaryKey
    private String email;
    @Required
    private String password;
    @Required
    private String phoneNo;

    private String firstName;
    private String lastName;

    @Required
    private String type;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public UserType getType() {
        return Enum.valueOf(UserType.class, type);
    }

    public void setType(UserType type) {
        this.type = type.toString();
    }
}
