package co.bakko.quiz.contact;

import android.content.Context;

import co.bakko.quiz.model.UserInfo;

public interface SignUpContact {

    interface View {

        void showErrorMsgForEmailId(boolean isError);

        void showErrorMsgForPassword(boolean isError);

        void showErrorMsgForPhoneNo(boolean isError);

        void showMsgForMissingData(String msg);

        void showSpinner();

        void showProgressView(boolean show);

        void forwardToMainView(String userId);
    }

    interface UserActionListener {

        void onStart(Context context);

        void signUpAction(UserInfo userInfo);

        void onDestroy();
    }
}
