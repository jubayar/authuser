package co.bakko.quiz.presenter;

import co.bakko.quiz.callback.QuizCallback;
import co.bakko.quiz.contact.MainViewContact;
import co.bakko.quiz.model.UserInfo;
import co.bakko.quiz.model.UserType;
import co.bakko.quiz.realm.dao.UserDao;

public class MainViewPresenter implements MainViewContact.UserActionListener {

    private MainViewContact.View view;
    private UserDao userRepo;
    private UserType userType;

    private UserInfo userInfo;

    public MainViewPresenter(MainViewContact.View view) {
        this.view = view;
        userRepo = new UserDao();
    }

    @Override
    public void onStart(String emailId) {
        userRepo.getUserByEmailId(emailId, new QuizCallback<UserInfo>() {
            @Override
            public void onSuccess(UserInfo info) {
                view.showUserInformation(info);
                userType = info.getType();

                userInfo = info;
            }

            @Override
            public void onError(String message) {

            }
        });
    }

    @Override
    public void editPhoneNoAction(String userId, String newPhoneNo) {
        userRepo.updateUserPhoneNo(userId, newPhoneNo, new QuizCallback<Boolean>() {
            @Override
            public void onSuccess(Boolean success) {
                //view.
            }

            @Override
            public void onError(String message) {

            }
        });
    }

    @Override
    public void userTypeAction() {
        view.showToastMsg("Your account type is " + userType.toString());
        view.showUserInformation(userInfo);
    }

    @Override
    public void logout() {
        view.showProgressDialog();
    }

    @Override
    public void onDestroy() {
        view = null;
        userRepo = null;
    }
}
