package co.bakko.quiz.presenter;

import android.content.Context;

import co.bakko.quiz.R;
import co.bakko.quiz.callback.SignUpCallback;
import co.bakko.quiz.contact.SignUpContact;
import co.bakko.quiz.model.UserInfo;
import co.bakko.quiz.realm.dao.UserDao;
import co.bakko.quiz.util.Tools;

public class SignUpPresenter implements SignUpContact.UserActionListener {

    private SignUpContact.View view;
    private Context context;
    private UserDao userRepo;

    public SignUpPresenter(SignUpContact.View view) {
        this.view = view;
        userRepo = new UserDao();
    }

    @Override
    public void onStart(Context context) {
        this.context = context;
        view.showSpinner();
    }

    @Override
    public void signUpAction(UserInfo userInfo) {

        if (!Tools.isFillUpBasicUserInfo(userInfo)) {
            view.showMsgForMissingData(context.getString(R.string.please_complete_the_form));
            return;
        }

        if (!Tools.isEmailValid(userInfo.getEmail())) {
            view.showErrorMsgForEmailId(true);
            return;
        }

        if (!Tools.isPasswordValid(userInfo.getPassword())) {
            view.showErrorMsgForPassword(true);
            return;
        }

        if (!Tools.isPhoneNoValid(userInfo.getPhoneNo())) {
            view.showErrorMsgForPhoneNo(true);
            return;
        }

        view.showProgressView(true);

        userRepo.addUser(userInfo, new SignUpCallback() {
            @Override
            public void onSuccess(boolean userSaved, String userId) {
                if (userSaved) {
                    view.showProgressView(false);
                    view.forwardToMainView(userId);
                } else {
                    view.showProgressView(false);
                    view.showMsgForMissingData("Failed to sign up");
                }
            }

            @Override
            public void onError(String message) {
                view.showProgressView(false);
                view.showMsgForMissingData(message);
            }
        });

    }

    @Override
    public void onDestroy() {
        view = null;
        userRepo = null;
        context = null;
    }
}
