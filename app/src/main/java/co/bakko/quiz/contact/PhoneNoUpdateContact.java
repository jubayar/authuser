package co.bakko.quiz.contact;

import android.support.v4.app.Fragment;

public interface PhoneNoUpdateContact {

    interface View {

        void showPhoneNoErrorMsg(boolean isError);

        void dismissDialog();
    }

    interface UserActionListener {

        void onStart(Fragment fragment, String emailId);

        void updatePhoneNo(String phoneNo);

        void sendIntentPhoneNo(String phoneNo);

        void onDestroy();
    }
}
