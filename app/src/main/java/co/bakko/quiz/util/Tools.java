package co.bakko.quiz.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import co.bakko.quiz.model.UserInfo;

public class Tools {

    private Tools() {}

    public static boolean isEmailValid(String email) {
        return email.contains("@");
    }

    public static boolean isPasswordValid(String password) {

        if (password.length() >= 8) {

            Pattern pattern = Pattern.compile("[^A-Za-z0-9]");
            Matcher matcher = pattern.matcher(password);
            return matcher.find();
        }

        return false;
    }

    public static boolean isFillUpBasicUserInfo(UserInfo user) {

        if ( isLengthZero(user.getEmail()) || isLengthZero(user.getPassword()) || isLengthZero(user.getPhoneNo()) || isLengthZero(user.getType().toString())) {
            return false;
        }
        return true;
    }

    private static boolean isLengthZero(String str) {
        return str.trim().length() == 0;
    }

    public static boolean isPhoneNoValid(String phoneNo) {

        String regex = "^6?01\\d{8}$";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(phoneNo);

        return matcher.matches();
    }

}
