package co.bakko.quiz.contact;

import android.content.Context;

public interface LoginContact {

    interface View {

        void showEmailIdErrorMsg(boolean isError);

        void showPasswordErrorMsg(boolean isError);

        void showMsgInSnackBar(String msg);

        void showProgressIndicator(boolean show);

        void forwardToMainView(String userId);
    }

    interface UserActionListener {

        void onStart(Context context);

        void loginAction(String emailId, String password);

        void onDestroy();
    }
}
