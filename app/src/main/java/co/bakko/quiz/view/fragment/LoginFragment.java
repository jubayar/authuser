package co.bakko.quiz.view.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import co.bakko.quiz.R;
import co.bakko.quiz.callback.SnackBarCallback;
import co.bakko.quiz.contact.LoginContact;
import co.bakko.quiz.presenter.LoginPresenter;
import co.bakko.quiz.view.activity.MainViewActivity;
import co.bakko.quiz.view.activity.AuthActivity;

public class LoginFragment extends Fragment implements LoginContact.View {

    private LoginContact.UserActionListener userActionListener;

    private EditText mEmailView;
    private EditText mPasswordView;
    private View mProgressView;
    private View mLoginFormView;
    private Button mLoginButton;

    private SnackBarCallback callback;

    public LoginFragment() { }

    public static LoginFragment newInstance() {
        LoginFragment fragment = new LoginFragment();
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof AuthActivity)
            callback = (AuthActivity)context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        userActionListener = new LoginPresenter(this);
        userActionListener.onStart(getContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_login, container, false);

        mEmailView = view.findViewById(R.id.email);
        mPasswordView = view.findViewById(R.id.password);

        mProgressView = view.findViewById(R.id.login_progress);
        mLoginFormView = view.findViewById(R.id.email_login_form);

        mLoginButton = view.findViewById(R.id.email_sign_in_button);

        mLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                userActionListener.loginAction(mEmailView.getText().toString(), mPasswordView.getText().toString());
            }
        });

        return view;
    }

    @Override
    public void showEmailIdErrorMsg(boolean isError) {
        if (isError) {
            mEmailView.setError(getString(R.string.error_invalid_email));
            mEmailView.requestFocus();
        } else {
            mEmailView.setError(null);
        }
    }

    @Override
    public void showPasswordErrorMsg(boolean isError) {
        if (isError) {
            mPasswordView.setError(getString(R.string.error_incorrect_password));
            mPasswordView.requestFocus();
        } else {
            mPasswordView.setError(null);
        }
    }

    @Override
    public void showMsgInSnackBar(String msg) {
        callback.showSnackBar(msg);
    }

    @Override
    public void showProgressIndicator(boolean show) {
        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
        mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
    }

    @Override
    public void forwardToMainView(String userId) {
        MainViewActivity.start(getContext(), userId);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        callback = null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        userActionListener.onDestroy();
    }
}
