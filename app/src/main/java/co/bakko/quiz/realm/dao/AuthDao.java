package co.bakko.quiz.realm.dao;

import co.bakko.quiz.callback.QuizCallback;
import co.bakko.quiz.model.AuthData;
import io.realm.Realm;

public class AuthDao implements IAuthDao {

    @Override
    public void checkValidUser(AuthData data, QuizCallback<String> callback) {

        Realm realmIns = null;

        try {
            realmIns = Realm.getDefaultInstance();
            AuthData authData = realmIns.where(AuthData.class).equalTo("emailId", data.getEmailId()).equalTo("password", data.getPassword()).findFirst();

            if (authData != null) {
                callback.onSuccess(authData.getEmailId());
            } else {
                callback.onError("User does not exist");
            }
        } finally {
            realmIns.close();
        }
    }

    @Override
    public void addUserLoginData(AuthData data, QuizCallback<Boolean> callback) {

        Realm realmObj = null;

        try {
            realmObj = Realm.getDefaultInstance();
            realmObj.executeTransactionAsync(
                    realm -> realm.copyToRealmOrUpdate(data),
                    () -> callback.onSuccess(true),
                    error -> callback.onSuccess(false));
        } finally {
            realmObj.close();
        }
    }
}
