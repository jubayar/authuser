package co.bakko.quiz.realm.dao;

import co.bakko.quiz.callback.QuizCallback;
import co.bakko.quiz.callback.SignUpCallback;
import co.bakko.quiz.model.UserInfo;

public interface IUserDao {

    void addUser(UserInfo userInfo, SignUpCallback callback);

    void getUserByEmailId(String emailId, QuizCallback<UserInfo> callback);

    void checkUserExistByEmailId(String emailId, QuizCallback<UserInfo> callback);

    void updateUserPhoneNo(String userId, String newPhoneNo, QuizCallback<Boolean> updateCallback);
}
