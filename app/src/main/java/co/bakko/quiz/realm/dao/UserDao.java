package co.bakko.quiz.realm.dao;

import co.bakko.quiz.callback.QuizCallback;
import co.bakko.quiz.callback.SignUpCallback;
import co.bakko.quiz.model.AuthData;
import co.bakko.quiz.model.UserInfo;
import io.realm.Realm;

public class UserDao implements IUserDao {

    private IAuthDao authRepo;

    public UserDao() {
        authRepo = new AuthDao();
    }

    @Override
    public void addUser(final UserInfo userInfo, final SignUpCallback callback) {
        Realm realmIns = null;

        try {
            realmIns = Realm.getDefaultInstance();
            UserInfo info = realmIns.where(UserInfo.class).equalTo("email", userInfo.getEmail()).findFirst();

            if (info != null) {
                callback.onError("Email ID is already exist.");
                return;
            }

            realmIns.executeTransactionAsync(
                    realm -> realm.insertOrUpdate(userInfo),
                    () -> {
                        AuthData authData = new AuthData(userInfo.getEmail(), userInfo.getPassword());
                        authRepo.addUserLoginData(authData, new QuizCallback<Boolean>() {
                            @Override
                            public void onSuccess(Boolean result) {
                                callback.onSuccess(true, userInfo.getEmail());
                            }
                            @Override
                            public void onError(String message) {
                                callback.onError("Sign up error");
                            }
                        });
                    },
                    error -> callback.onError("Sign up error")
            );

        } finally {
            realmIns.close();
        }
    }

    @Override
    public void getUserByEmailId(String emailId, QuizCallback<UserInfo> callback) {
        Realm realmIns = null;

        try {
            realmIns = Realm.getDefaultInstance();

            UserInfo userInfo = realmIns.where(UserInfo.class).equalTo("email", emailId).findFirst();

            if (userInfo != null) {
                callback.onSuccess(realmIns.copyFromRealm(userInfo));
            } else {
                callback.onError("Not available");
            }
        } finally {
            realmIns.close();
        }
    }

    @Override
    public void checkUserExistByEmailId(String emailId, QuizCallback<UserInfo> callback) {
        Realm realmIns = null;

        try {
            realmIns = Realm.getDefaultInstance();

            UserInfo userInfo = realmIns.where(UserInfo.class).equalTo("email", emailId).findFirst();

            if (userInfo != null) {
                callback.onSuccess(realmIns.copyFromRealm(userInfo));
            } else {
                callback.onSuccess(null);
            }
        } finally {
            realmIns.close();
        }
    }

    @Override
    public void updateUserPhoneNo(String emailId, String newPhoneNo, QuizCallback callback) {

        Realm realmObj = null;

        try {
            realmObj = Realm.getDefaultInstance();
            realmObj.executeTransactionAsync(
                    realm -> {
                        UserInfo userInfo = realm.where(UserInfo.class).equalTo("email", emailId).findFirst();
                        userInfo.setPhoneNo(newPhoneNo);
                    },
                    () -> callback.onSuccess(true),
                    error -> callback.onSuccess(false));
        } finally {
            realmObj.close();
        }
    }
}
