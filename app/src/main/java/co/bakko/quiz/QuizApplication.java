package co.bakko.quiz;

import android.app.Application;

import co.bakko.quiz.realm.module.QuizModule;
import io.realm.Realm;
import io.realm.RealmConfiguration;

public class QuizApplication extends Application {

    private static QuizApplication instance;

    @Override
    public void onCreate() {
        super.onCreate();
        
        instance = this;
        Realm.init(this);
        RealmConfiguration config = new RealmConfiguration.Builder().name("myrealm.realm").modules(new QuizModule()).build();
        Realm.setDefaultConfiguration(config);
    }

    public static QuizApplication getInstance() {
        return instance;
    }
}
